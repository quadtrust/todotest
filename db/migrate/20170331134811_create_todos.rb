class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.string :todo
      t.boolean :is_completed

      t.timestamps null: false
    end
  end
end
