# == Schema Information
#
# Table name: todos
#
#  id           :integer          not null, primary key
#  todo         :string(255)
#  is_completed :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Todo < ActiveRecord::Base
  validates :todo, presence: true
end
