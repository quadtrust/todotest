# == Schema Information
#
# Table name: todos
#
#  id           :integer          not null, primary key
#  todo         :string(255)
#  is_completed :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class TodosController < ApplicationController

  before_action :set_todo, only: [:edit, :update, :show, :destroy]

  def index
    render json: Todo.all
  end

  def new
    @todo = Todo.new
  end

  def create
    @todo = Todo.new(todo_params)
    if @todo.save
      render json: @todo
    else
      render json: {
          error: 'Some error occured!'
      }
    end
  end

  def show
    render json: @todo
  end

  def edit

  end

  def update
    if @todo.update(todo_params)
      render json: {
          success: 'ok'
      }
    else
      render json: {
          error: 'Some error occured!'
      }
    end
  end

  def destroy
    if @todo.destroy
      render json: {
          success: 'ok'
      }
    else
      render json: {
          error: 'Some error occured!'
      }
    end
  end

  private

  def todo_params
    params.require(:todo).permit(:todo, :is_completed)
  end

  def set_todo
    @todo = Todo.find(params[:id])
  end
end