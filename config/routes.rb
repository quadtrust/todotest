Rails.application.routes.draw do
  devise_for :users,  controllers: {sessions: 'users/sessions'}
  root 'home#index'
  resources :todos, only: [:index, :new, :create, :show, :edit, :update, :destroy]
end
